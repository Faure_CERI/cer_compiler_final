# CERIcompiler

A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)

**Download the repository :**

> git clone git@framagit.org:jourlin/cericompiler.git

**Build the compiler and test it :**

> make test

**Have a look at the output :**

> gedit test.s

**Debug the executable :**

> ddd ./test

**Commit the new version :**

> git commit -a -m "What's new..."

**Send to your framagit :**

> git push -u origin master

**Get from your framagit :**

> git pull -u origin master

**This version Can handle :**

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
// VarDeclaration := Ident {"," Ident} ":" Type

//<case statement> ::= case <expression> of <case list element> {; <case list element> } end 
//<case list element> ::= <case label list> : <statement> | <empty> 
//<case label list> ::= <constant> {, <constant> } 
//<empty>::=


//Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement
//IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
//WhileStatement := "WHILE" Expression DO Statement
//ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement
//BlockStatement := "BEGIN" Statement { ";" Statement } "END"


// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

à quelques détails près: La gestion des types float et char sont limités à leur déclaration et le case n'est pas terminé

